angular.module('starter.controllers', [])

.controller('MainController', [ '$scope', function($scope) {
	// $scope.toggleMenu = function() {
	// 	$scope.sideMenuController.toggleLeft();
	// };
}])

.controller('HomeController', ['$scope', '$state', function($scope, $state) {
	// $scope.navTitle = 'Home Page';
	// $scope.leftButtons = [{
	// 	type: 'button-icon icon ion-navicon',
	// 	tap: function(e) {
	// 		$scope.toggleMenu();
	// 	}
	// }];
}])

.controller('ListAccordionController', ['$scope', function($scope) {

	$scope.groups = [];
  for (var i=0; i<10; i++) {
    $scope.groups[i] = {
      name: i,
      items: []
    };
    for (var j=0; j<3; j++) {
      $scope.groups[i].items.push(i + '-' + j);
    }
  }

  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };

}])

.controller('TaskCtrl', ['$scope', function($scope) {
  $scope.close = function() {
    $scope.modal.hide();
  }
}])

.controller('InfiniteScrollListCtrl', ['$scope', '$ionicNavBarDelegate', function($scope, $ionicNavBarDelegate) {

  $scope.items = [];
  $scope.loadMore = function() {
    var data = [];
    var l = $scope.items.length
    for ( var i = l; i < l+20; i++) {
      data.push({id: i});
    }

    $scope.items = $scope.items.concat(data);
    $scope.$broadcast('scroll.infiniteScrollComplete');
    $ionicNavBarDelegate.setTitle('<span class="badge badge-assertive">' + $scope.items.length + '</span> Items loaded');
  };

  $scope.$on('stateChangeSuccess', function() {
    $scope.loadMore();
  });

}])

.controller('RadioCtrl', ['$scope', function($scope) {

  $scope.clientSideList = [
    { text: "Backbone", value: "bb" },
    { text: "Angular", value: "ng" },
    { text: "Ember", value: "em" },
    { text: "Knockout", value: "ko" }
  ];

  $scope.serverSideList = [
    { text: "Go", value: "go" },
    { text: "Python", value: "py" },
    { text: "Ruby", value: "rb" },
    { text: "Java", value: "jv" }
  ];

  $scope.data = {
    clientSide: 'ng'
  };

  $scope.serverSideChange = function(item) {
    console.log("Selected Serverside, text:", item.text, "value:", item.value);
  };

}])

.controller('ToggleController', function($scope) {

  $scope.data = {
    isLoading: false
  };

  $scope.settingsList = [
    { text: "Wireless", checked: true },
    { text: "GPS", checked: false },
    { text: "Bluetooth", checked: false }
  ];

  $scope.pushNotificationChange = function() {
    console.log('Push Notification Change', $scope.pushNotification.checked);
  };

  $scope.pushNotification = { checked: true };
  $scope.emailNotification = 'Subscribed';

})

.controller('PullToRefreshController', ['$scope', '$timeout', '$ionicModal', function($scope, $timeout, $ionicModal) {

	$scope.items = ['Item 1', 'Item 2', 'Item 3'];

  $ionicModal.fromTemplateUrl('templates/new-item.html', function(modal) {
    $scope.settingsModal = modal;
  });

  $scope.newTask = function() {
    $scope.settingsModal.show();
  };

	$scope.doRefresh = function() {
		console.log('Refreshing!');
		$timeout( function() {
			//simulate async response
			$scope.items.push('New Item ' + Math.floor(Math.random() * 1000) + 4);

			//Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
		}, 1000);
	};

}])

.controller('DashCtrl', ['$scope', '$ionicPopup', '$timeout', '$ionicActionSheet', '$ionicBackdrop', function($scope, $ionicPopup, $timeout, $ionicActionSheet, $ionicBackdrop) {

  $scope.showBackdrop = function() {
    $ionicBackdrop.retain();
    $timeout(function() {
      $ionicBackdrop.release();
    }, 1000);
  };

	$scope.showActionsheet = function() {

		$ionicActionSheet.show({
			titleText: 'ActionSheet Example',
			buttons: [
				{ text: 'Share <i class="icon ion-share"></i>' },
				{ text: 'Move <i class="icon ion-arrow-move"></i>' },
			],
			destructiveText: 'Delete',
			cancelText: 'Cancel',
			cancel: function() {
				console.log('CANCELLED');
			},
			buttonClicked: function(index) {
				console.log('BUTTON CLICKED', index);
				return true;
			},
			destructiveButtonClicked: function() {
				console.log('DESTRUCT');
				return true;
			}
		});
	};

 // Triggered on a button click, or some other target
 $scope.showPopup = function() {
   $scope.data = {}

   // An elaborate, custom popup
   var myPopup = $ionicPopup.show({
     template: '<input type="password" ng-model="data.wifi">',
     title: 'Enter Wi-Fi Password',
     subTitle: 'Please use normal things',
     scope: $scope,
     buttons: [
       { text: 'Cancel' },
       {
         text: '<b>Save</b>',
         type: 'button-positive',
         onTap: function(e) {
           if (!$scope.data.wifi) {
             //don't allow the user to close unless he enters wifi password
             e.preventDefault();
           } else {
             return $scope.data.wifi;
           }
         }
       },
     ]
   });
   myPopup.then(function(res) {
     console.log('Tapped!', res);
   });
   $timeout(function() {
      myPopup.close(); //close the popup after 3 seconds for some reason
   }, 3000);
  };
   // A confirm dialog
   $scope.showConfirm = function() {
     var confirmPopup = $ionicPopup.confirm({
       title: 'Consume Ice Cream',
       template: 'Are you sure you want to eat this ice cream?'
     });
     confirmPopup.then(function(res) {
       if(res) {
         console.log('You are sure');
       } else {
         console.log('You are not sure');
       }
     });
   };

   // An alert dialog
   $scope.showAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'Don\'t eat that!',
       template: 'It might taste good'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
   };

}])

.controller('FriendsCtrl', ['$scope', 'Friends', function($scope, Friends) {
  $scope.friends = Friends.all();
}])

.controller('FriendDetailCtrl', ['$scope', '$stateParams', 'Friends', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.friendId);
}])

.controller('AccountCtrl', ['$scope', '$ionicPopover', function($scope, $ionicPopover) {

	$ionicPopover.fromTemplateUrl('templates/popover.html', function(popover) {
		$scope.popover = popover;
	});

	$scope.demo = 'ios';
	$scope.setPlatform = function(p) {
		document.body.classList.remove('platform-ios');
		document.body.classList.remove('platform-android');
		document.body.classList.add('platform-' + p);
		$scope.demo = p;
	}
}])

.controller('AllCtrl', ['$scope', function($scope) {
	$scope.items = [1, 2, 3, 4, 5, 6];
}])

.controller('SlideCtrl', ['$scope', '$state', '$ionicSlideBoxDelegate', function($scope, $state, $ionicSlideBoxDelegate) {

  $scope.startApp = function() {
    $state.go('main.home');
  };

  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };

  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

	$scope.slideHasChanged = function(index) {
		$scope.slideIndex = index;
	};

}]);
