// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('main', {
      url: '/main',
      abstract: true,
      templateUrl: 'templates/side-menu.html',
      controller: 'MainController'
    })

    .state('main.home', {
      url: '/home',
      views: {
        'main': {
          templateUrl: 'templates/home.html',
          controller: 'HomeController'
        }
      }
    })

    .state('main.list_accordion', {
      url: '/list/accordion',
      views: {
        'main': {
          templateUrl: 'templates/list_accordion.html',
          controller: 'ListAccordionController'
        }
      }
    })

    .state('main.toggle', {
      url: '/toggle',
      views: {
        'main': {
          templateUrl: 'templates/toggle.html',
          controller: 'ToggleController'
        }
      }
    })

    // setup an abstract state for the tabs directive
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })

    .state('main.infinite_scroll_list', {
      url: '/list/infinite',
      views: {
        'main': {
          templateUrl: 'templates/infinite-scroll-list.html',
          controller: 'InfiniteScrollListCtrl'
        }
      }
    })

    .state('main.radio', {
      url: '/radio',
      views: {
        'main': {
          templateUrl: 'templates/radio.html',
          controller: 'RadioCtrl'
        }
      }
    })

    .state('tab.pull_to_refresh', {
      url: '/pull-to-refresh',
      views: {
        'tab-friends': {
          templateUrl: 'templates/pull-to-refresh.html',
          controller: 'PullToRefreshController'
        }
      }
    })

    // Each tab has its own nav history stack:

    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-dash.html',
          controller: 'DashCtrl'
        }
      }
    })

    .state('tab.friends', {
      url: '/friends',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'FriendsCtrl'
        }
      }
    })
    .state('tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      }
    })

    .state('tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl'
        }
      }
    })

    .state('tab.all', {
      url: '/all',
      views: {
        'tab-all': {
          templateUrl: 'templates/tab-all.html',
          controller: 'AllCtrl'
        }
      }
    })

    .state('tab.slide', {
      url: '/slide',
      views: {
        'tab-all': {
          templateUrl: 'templates/tab-slide.html',
          controller: 'SlideCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/main/home');

});

